package fr.nathanael2611.roleplaychat.plugin.core;

/**
 * Title : EnumChatTypes
 * Description : Enumerates all chat types
 * Developer : @Nathanael2611
 */
public enum EnumChatTypes
{
    HRP, NORMAL, SHOUT, WISP,ACTION
}
