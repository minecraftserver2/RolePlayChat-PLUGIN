package fr.nathanael2611.roleplaychat.plugin.core;

import org.json.JSONObject;
import fr.nathanael2611.roleplaychat.plugin.RolePlayChat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * Title : RolePlayNames
 * Description : Handle players rp names
 * Developer : @Nathanael2611
 */
public class RolePlayNames
{
    /**
     * Read roleplaynames JSON file in config folder and create it if doesn't exist
     * @return JSON object from RolePlayNamesFile
     */
    public static JSONObject getAllRPChatFile()
    {
        String json = readFileToString(RolePlayChat.getRoleplayNamesFile());
        if (!json.startsWith("{") && !json.endsWith("}"))
        {
            try
            {
                final FileWriter writer = new FileWriter(RolePlayChat.getRoleplayNamesFile());
                writer.write("{}");
                writer.close();
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return new JSONObject(readFileToString(RolePlayChat.getRoleplayNamesFile()));
    }

    /**
     * Put new rp name in roleplaynames JSON file
     * @param player targeted player
     * @param rpName new custom role play name
     */
    public static void setRPName(final String player, final String rpName)
    {
        final JSONObject jsonObject = getAllRPChatFile();
        jsonObject.put(player, rpName);
        try
        {
            final FileWriter writer = new FileWriter(RolePlayChat.getRoleplayNamesFile());
            writer.write(jsonObject.toString());
            writer.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * @param player targeted player
     * @return player's custom rp name
     */
    public static String getRPName(final String player)
    {
        final JSONObject jsonObject = getAllRPChatFile();
        if (jsonObject.isNull(player))
        {
            return player;
        }
        return jsonObject.getString(player);
    }

    /**
     * @param file read file using deprecated systems
     * @return file to string
     */
    public static String readFileToString(final File file)
    {
        try
        {
            final BufferedReader reader = new BufferedReader(new FileReader(file));
            final StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            final String ls = System.getProperty("line.separator");
            while ((line = reader.readLine()) != null)
            {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            reader.close();
            return stringBuilder.toString();
        } catch (Exception e)
        {
            e.printStackTrace();
            return "ERROR";
        }
    }

}
