package fr.nathanael2611.roleplaychat.plugin.core;

/**
 * Title : ChatType
 * Description : Provide the specificities of each chat type
 * Developer : @Nathanael2611
 */
public class ChatType
{

    private String prefix;
    private int distance;
    private String format;

    /**
     * ChatType Builder
     * @param prefix Prefix to use this chat type
     * @param distance Maximum distance with this chat type
     * @param format Text format for this chat type
     */
    public ChatType(String prefix, int distance, String format)
    {
        this.prefix = prefix;
        this.distance = distance;
        this.format = format;
    }

    /**
     * Get chat type prefix
     * @return Chat type prefix
     */
    public String getPrefix()
    {
        return prefix;
    }

    /**
     * Get chat type distance
     * @return Chat type distance
     */
    public int getDistance()
    {
        return distance;
    }

    /**
     * Get chat type format
     * @return Chat type format
     */
    public String getFormat()
    {
        return format;
    }
}
