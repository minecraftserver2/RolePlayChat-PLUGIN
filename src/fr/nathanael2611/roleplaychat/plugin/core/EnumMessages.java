package fr.nathanael2611.roleplaychat.plugin.core;

/**
 * Title : EnumMessages
 * Description : Enumerates all messages
 * Developer : @Nathanael2611
 */
public enum EnumMessages {
    SETRPNAME, SETRPNAMEOTHER, NOPERM
}
